package br.com.lapidar_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LapidarSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(LapidarSpringApplication.class, args);
	}

}

